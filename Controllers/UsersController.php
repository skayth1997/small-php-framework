<?php

namespace App\Controllers;

use App\Core\App;

class UsersController
{
    public function store()
    {
        App::get('database')->insert('users', [
            'name' => $_POST['name'],
            'surname' => $_POST['surname'],
        ]);

        redirect('/');
    }
}