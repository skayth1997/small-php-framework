<?php

namespace App\Controllers;

use App\Core\App;

class IndexController
{
    public function index()
    {
        $users = App::get('database')->selectAll('users');
        view('index', ['users' => $users]);
    }
}