<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Small Framework</title>
</head>
<body>

<nav>
    <ul>
        <li><a href="/home">Home</a></li>
        <li><a href="/blog">Blog</a></li>
        <li><a href="/about">About</a></li>
        <li><a href="/contact">Contact</a></li>
    </ul>
</nav>

<form action="/users" method="POST">
    <input name="name" type="text" placeholder="name"/><br/>
    <input name="surname" type="text" placeholder="surname"/><br/>
    <button>Submit</button>
</form>
