<?php

use App\Core\Route;
use App\Core\Request;

require "vendor/autoload.php";
require "Core/bootstrap.php";

Route::load('Routes/routes.php')
    ->direct(Request::uri(), Request::method());
