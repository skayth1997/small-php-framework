<?php

use App\Core\App;
use App\Core\Database\QueryBuilder;
use App\Core\Database\Connection;

require "Helper/functions.php";

App::bind('config', require "Config/config.php");

App::bind('database', new QueryBuilder(
    Connection::make(
        App::get('config')['database']
    )
));