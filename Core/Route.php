<?php

namespace App\Core;

class Route
{
    /**
     * @var array
     */
    private $routes = [
        'GET' => [],
        'POST' => [],
    ];

    public static function load(string $file)
    {
        $router = new self;

        require $file;

        return $router;
    }

    /**
     * @param $uri
     * @param $controller
     */
    public function get($uri, $controller)
    {
        $this->routes['GET'][$uri] = $controller;
    }

    /**
     * @param $uri
     * @param $controller
     */
    public function post($uri, $controller)
    {
        $this->routes['POST'][$uri] = $controller;
    }

    /**
     * @param $uri
     * @param $requestType
     * @throws Exception
     */
    public function direct($uri, $requestType)
    {
        $this->callAction(
            ...explode('@', $this->routes[$requestType][$uri])
        );
    }

    /**
     * @param $controller
     * @param $method
     * @throws Exception
     */
    private function callAction($controller, $method)
    {
        $controller = "App\Controllers\\{$controller}";
        $controller = new $controller;

        if (!method_exists($controller, $method)) {
            throw new Exception("{$controller} does not respond to the {$method} method");
        }

        $controller->$method();
    }
}