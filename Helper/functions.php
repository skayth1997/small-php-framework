<?php

function pr($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

function dd($data)
{
    die(var_dump($data));
}

function view($name, $data = [])
{
    extract($data);

    require "Views/{$name}.view.php";
}

function redirect($path)
{
    header("Location: {$path}");
}