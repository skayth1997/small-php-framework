<?php

$router->get('', 'IndexController@index');
$router->get('home', 'IndexController@index');
$router->get('blog', 'BlogController@index');
$router->get('about', 'AboutController@index');
$router->get('contact', 'ContactsController@index');
$router->post('users', 'UsersController@store');