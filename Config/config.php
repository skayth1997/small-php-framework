<?php

return [
    'database' => [
        'name' => 'small_framework',
        'username' => 'root',
        'password' => 'kapriz',
        'connection' => 'mysql:host=127.0.0.1',
        'options' => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING,
        ],
    ],
];